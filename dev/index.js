/* eslint-disable import/no-extraneous-dependencies */
/**
 * @format
 */
import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as appName } from './src/app.json';
import 'core-js/fn/symbol/iterator';

global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');

AppRegistry.registerComponent(appName, () => App);
