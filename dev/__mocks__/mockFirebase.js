/* eslint-disable no-undef */
jest.mock('firebase/app', () => {
  const data = {
    name: 'unnamed',
  };
  const snapshot = {
    val: () => data,
  };
  return {
    initializeApp: jest.fn().mockReturnValue({
      database: jest.fn().mockReturnValue({
        ref: jest.fn().mockReturnThis(),
        once: jest.fn(() => Promise.resolve(snapshot)),
        on: jest.fn(() => Promise.resolve(snapshot)),
        push: jest.fn(() => Promise.resolve(snapshot)),
        child: jest.fn().mockReturnValue({
          set: jest.fn(() => Promise.resolve(snapshot)),
        }),
      }),
    }),
  };
});
