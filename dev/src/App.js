import React from 'react';
import { Router, Scene } from 'react-native-router-flux';

import FlashMessage from 'react-native-flash-message';
import { Provider } from 'react-redux';
import { View } from 'react-native';
import Aluno from './components/view/aluno';
import Cadatro from './components/view/cadastro';
import Login from './components/view/login';
import Menu from './components/view/menu';
import Motorista from './components/view/motorista';
import Rota from './components/view/rota';
import Onibus from './components/view/veiculo';
import configureStore from './store';

const store = configureStore();
const App = () => {
  return (
    <View style={{ flex: 1 }}>
      <Provider store={store}>
        <Router>
          <Scene key="root">
            <Scene key="login" component={Login} hideNavBar initial />
            <Scene key="menu" component={Menu} hideNavBar />
            <Scene key="cadastro" component={Cadatro} hideNavBar />
            <Scene key="aluno" component={Aluno} hideNavBar />
            <Scene key="motorista" component={Motorista} hideNavBar />
            <Scene key="rota" component={Rota} hideNavBar />
            <Scene key="onibus" component={Onibus} hideNavBar />
          </Scene>
        </Router>
      </Provider>
      <FlashMessage position="top" autoHide duration={5000} />
    </View>
  );
};

export default App;
