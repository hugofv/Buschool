import { FETCH_ALUNOS } from '../actions/types';

const initialState = {
  alunos: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALUNOS: {
      const { alunos } = action;
      return Object.assign({}, { ...state, alunos: alunos || [] });
    }
    default: {
      return state;
    }
  }
};
