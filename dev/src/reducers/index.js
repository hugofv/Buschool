import { combineReducers } from 'redux';

import aluno from './aluno';
import motorista from './motorista';
import rota from './rota';
import veiculo from './veiculo';
import login from './login';

export default combineReducers({
  aluno,
  motorista,
  rota,
  veiculo,
  login,
});
