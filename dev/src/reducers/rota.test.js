/* eslint-disable no-undef */
import rota from './rota';

describe('rota reducer', () => {
  test('Should handle FETCH_ROTA', () => {
    const action = {
      type: 'FETCH_ROTA',
      rota: { rotas: [] },
    };

    expect(rota(undefined, action)).toEqual({ rotas: [] });
  });
});
