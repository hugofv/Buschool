import { FETCH_ROTAS } from '../actions/types';

const initialState = {
  rotas: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ROTAS: {
      const { rotas } = action;
      return Object.assign({}, { ...state, rotas: rotas || [] });
    }
    default: {
      return state;
    }
  }
};
