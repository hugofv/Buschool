/* eslint-disable no-undef */
import motorista from './motorista';

describe('motorista reducer', () => {
  test('Should handle FETCH_MOTORISTA', () => {
    const action = {
      type: 'FETCH_MOTORISTA',
      motorista: { motoristas: [] },
    };

    expect(motorista(undefined, action)).toEqual({ motoristas: [] });
  });
});
