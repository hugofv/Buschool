import { FETCH_VEICULOS } from '../actions/types';

const initialState = {
  veiculos: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_VEICULOS: {
      const { veiculos } = action;
      return Object.assign({}, { ...state, veiculos: veiculos || [] });
    }
    default: {
      return state;
    }
  }
};
