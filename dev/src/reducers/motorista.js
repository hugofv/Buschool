import { FETCH_MOTORISTAS } from '../actions/types';

const initialState = {
  motoristas: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MOTORISTAS: {
      const { motoristas } = action;
      return Object.assign({}, { ...state, motoristas: motoristas || [] });
    }
    default: {
      return state;
    }
  }
};
