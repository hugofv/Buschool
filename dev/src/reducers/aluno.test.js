/* eslint-disable no-undef */
import aluno from './aluno';

describe('aluno reducer', () => {
  test('Should handle FETCH_ALUNO', () => {
    const action = {
      type: 'FETCH_ALUNO',
      aluno: { alunos: [] },
    };

    expect(aluno(undefined, action)).toEqual({ alunos: [] });
  });
});
