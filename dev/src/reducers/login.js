/* eslint-disable no-use-before-define */
/* eslint-disable no-var */
import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';
import { LOGIN_SUCCESS } from '../actions/types';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      const { user } = action;

      setStorage(user);
      getStorage();

      Actions.menu.call();
      return {
        ...state,
        user,
      };
    }
    default: {
      return state;
    }
  }
};

const setStorage = async (user) => {
  try {
    await AsyncStorage.setItem('UserPerfil', JSON.stringify(user.perfil));
  } catch (error) {
    console.error(error);
  }
};

const getStorage = async () => {
  try {
    const value = await AsyncStorage.getItem('UserPerfil');
    if (value !== null) {
      console.log('returned:', value);
    }
  } catch (error) {
    console.error(error);
  }
};
