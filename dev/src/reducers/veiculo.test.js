/* eslint-disable no-undef */
import veiculo from './veiculo';

describe('veiculo reducer', () => {
  test('Should handle FETCH_VEICULO', () => {
    const action = {
      type: 'FETCH_VEICULO',
      veiculo: { veiculos: [] },
    };

    expect(veiculo(undefined, action)).toEqual({ veiculos: [] });
  });
});
