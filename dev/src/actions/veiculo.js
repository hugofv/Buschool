/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
/* eslint-disable no-use-before-define */
import { FETCH_VEICULOS, FETCH_VEICULOS_ERROR } from './types';
import { showMessage } from 'react-native-flash-message';

import { firebase } from '../config/db';

export const fetchVeiculos = () => (dispatch) => {
  const itens = [];

  firebase
    .database()
    .ref('/veiculos')
    .on(
      'value',
      (snap) => {
        snap.forEach((snapChild) => {
          const item = snapChild.val();
          item.key = snapChild.key;
          itens.push(item);
        });

        dispatch(fetchVeiculoFinished(itens));
      },
      (erro) => dispatch(fetchVeiculoError()),
    );
};

export const addVeiculo = (veiculo) => (dispatch) => {
  try {
    if (veiculo && !veiculo.id) {
      firebase
        .database()
        .ref('/veiculos')
        .push(veiculo).then(()=>{
          showMessage({
            message: 'Cadastro realizado com sucesso!',
            type: 'success',
          });
        });
    } else {
      const { id } = veiculo;
      delete veiculo.id;

      firebase
        .database()
        .ref()
        .child(`/veiculos/${id}`)
        .set(veiculo)
        .then(() => {
          dispatch(fetchVeiculos())
        });
    }
  } finally {
    //fetchVeiculos();
  }
};

export const deleteVeiculo = (veiculo) => (dispatch) => {
  try {
    if (veiculo && veiculo.key) {
      firebase
        .database()
        .ref('/veiculos')
        .child(veiculo.key)
        .remove()
        .then(() => {
          dispatch(fetchVeiculos())
        })
    }
  } finally {
    
  }
};

const fetchVeiculoFinished = (veiculos) => ({
  type: FETCH_VEICULOS,
  veiculos,
});

const fetchVeiculoError = () => ({
  type: FETCH_VEICULOS_ERROR,
});
