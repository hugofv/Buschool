/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
/* eslint-disable no-use-before-define */
import { FETCH_MOTORISTAS, FETCH_MOTORISTAS_ERROR } from './types';
import { showMessage } from 'react-native-flash-message';


import { firebase } from '../config/db';

export const fetchMotoristas = () => (dispatch) => {        
  const itens = [];

  firebase
    .database()
    .ref('/motoristas')
    .on(
      'value',
      (snap) => {
        snap.forEach((snapChild) => {
          const item = snapChild.val();
          item.key = snapChild.key;

          itens.push(item);
        });

        dispatch(fetchMotoristaFinished(itens));
      },
      (erro) => dispatch(fetchMotoristaError()),
    );
};

export const addMotorista = (motorista) => (dispatch) => {
  try {
    if (motorista && !motorista.id) {
      firebase
        .database()
        .ref('/motoristas')
        .push(motorista).then(()=>{
          showMessage({
            message: 'Cadastro realizado com sucesso!',
            type: 'success',
          });
        });
    } else {
      const { id } = motorista;
      delete motorista.id;

      firebase
        .database()
        .ref()
        .child(`/motoristas/${id}`)
        .set(motorista)
        .then(() => {
          dispatch(fetchMotoristas())
        });
    }
  } finally {
    //fetchMotoristas();
  }
};

export const deleteMotorista = (motorista) => (dispatch) => {
  
  try {
    if (motorista && motorista.key) {

      firebase
        .database()
        .ref('/motoristas')
        .child(motorista.key)
        .remove()
        .then(() => {
          dispatch(fetchMotoristas())
        })
    }
  } finally {
    
  }
};

const fetchMotoristaFinished = (motoristas) => ({
  type: FETCH_MOTORISTAS,
  motoristas,
});

const fetchMotoristaError = () => ({
  type: FETCH_MOTORISTAS_ERROR,
});
