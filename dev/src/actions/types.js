export const ADD_ALUNO = 'ADD_ALUNO';
export const FETCH_ALUNOS = 'FETCH_ALUNOS';
export const FETCH_ALUNOS_ERROR = 'FETCH_ALUNOS_ERROR';

export const ADD_MOTORISTA = 'ADD_MOTORISTA';
export const FETCH_MOTORISTAS = 'FETCH_MOTORISTAS';
export const FETCH_MOTORISTAS_ERROR = 'FETCH_MOTORISTAS_ERROR';

export const ADD_ROTA = 'ADD_ROTA';
export const FETCH_ROTAS = 'FETCH_ROTAS';
export const FETCH_ROTAS_ERROR = 'FETCH_ROTAS_ERROR';

export const ADD_VEICULO = 'ADD_VEICULO';
export const FETCH_VEICULOS = 'FETCH_VEICULOS';
export const FETCH_VEICULOS_ERROR = 'FETCH_VEICULOS_ERROR';


export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
