/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
/* eslint-disable no-use-before-define */
import { FETCH_ROTAS, FETCH_ROTAS_ERROR } from './types';
import { showMessage } from 'react-native-flash-message';

import { firebase } from '../config/db';

export const fetchRotas = () => (dispatch) => {
  const itens = [];

  firebase
    .database()
    .ref('/rotas')
    .on(
      'value',
      (snap) => {
        snap.forEach((snapChild) => {
          const item = snapChild.val();
          item.key = snapChild.key;
          itens.push(item);
        });

        dispatch(fetchRotaFinished(itens));
      },
      (erro) => dispatch(fetchRotaError()),
    );
};

export const addRota = (rota) => (dispatch) => {
  try {
    if (rota && !rota.id) {
      firebase
        .database()
        .ref('/rotas')
        .push(rota).then(()=>{
          showMessage({
            message: 'Cadastro realizado com sucesso!',
            type: 'success',
          });
        });
    } else {
      const { id } = rota;
      delete rota.id;

      firebase
        .database()
        .ref()
        .child(`/rotas/${id}`)
        .set(rota)
        .then(() => {
          dispatch(fetchRotas())
        });
    }
  } finally {
    //fetchRotas();
  }
};

export const deleteRota = (rota) => (dispatch) => {
  try {
    if (rota && rota.key) {
      firebase
        .database()
        .ref('/rotas')
        .child(rota.key)
        .remove()
        .then(() => {
          dispatch(fetchRotas())
        })
    }
  } finally {
    
  }
};

const fetchRotaFinished = (rotas) => ({
  type: FETCH_ROTAS,
  rotas,
});

const fetchRotaError = () => ({
  type: FETCH_ROTAS_ERROR,
});
