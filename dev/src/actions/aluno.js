/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
/* eslint-disable no-use-before-define */
import { FETCH_ALUNOS, FETCH_ALUNOS_ERROR } from './types';
import { showMessage } from 'react-native-flash-message';

import { firebase } from '../config/db';

export const fetchAlunos = () => (dispatch) => {
  const itens = [];

  firebase
    .database()
    .ref('/alunos')
    .on(
      'value',
      (snap) => {
        snap.forEach((snapChild) => {
          const item = snapChild.val();
          item.key = snapChild.key;
          itens.push(item);
        });

        dispatch(fetchAlunoFinished(itens));
      },
      (erro) => dispatch(fetchAlunoError()),
    );
};

export const addAluno = (aluno) => (dispatch) => {
  try {
    if (aluno && !aluno.id) {
      firebase
        .database()
        .ref('/alunos')
        .push(aluno).then(()=>{
          showMessage({
            message: 'Cadastro realizado com sucesso!',
            type: 'success',
          });
        });
    } else {
      const { id } = aluno;
      delete aluno.id;

      firebase
        .database()
        .ref()
        .child(`/alunos/${id}`)
        .set(aluno)
        .then(() => {
          dispatch(fetchAlunos())
        });
    }
  } finally {
    //fetchAlunos();
  }
};

export const deleteAluno = (aluno) => (dispatch) => {
  try {
    if (aluno && aluno.key) {
      firebase
        .database()
        .ref('/alunos')
        .child(aluno.key)
        .remove()
        .then(() => {
          dispatch(fetchAlunos())
        })
    }
  } finally {
    
  }
};

const fetchAlunoFinished = (alunos) => ({
  type: FETCH_ALUNOS,
  alunos,
});

const fetchAlunoError = () => ({
  type: FETCH_ALUNOS_ERROR,
});
