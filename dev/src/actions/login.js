import { showMessage } from 'react-native-flash-message';

import { LOGIN_SUCCESS } from './types';
import { firebase } from '../config/db';

export const autenticar = (email, password) => (dispatch) => {
  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then((data) => {
      firebase
        .database()
        .ref('/usuarios')
        .orderByChild('email')
        .equalTo(data.user.email)
        .on('value', (snap) => {
          snap.forEach((snapChild) => {
            const item = snapChild.val();
            item.key = snapChild.key;
            global.teste = snapChild.val();

            dispatch({
              type: LOGIN_SUCCESS,
              user: item,
            });
            showMessage({
              message: 'Login realizado com sucesso!',
              type: 'success',
            });
          });
        });
    })
    .catch(() => {
      showMessage({
        message: 'Ocorreu um erro',
        type: 'danger',
      });
    });
};
