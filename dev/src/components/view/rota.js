import { Button, Container, Content, DatePicker, Form, Input, Item, Left, List, ListItem, Right,} from 'native-base';
import {Dimensions, Image, StyleSheet, Text, TouchableOpacity, View, ScrollView, } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withFormik } from 'formik';
import { addRota, deleteRota, fetchRotas } from '../../actions/rota';
import editIcon from './../../assets/img/edit32.png';
import deleteIcon from './../../assets/img/delete32.png';
import addIcon from './../../assets/img/addIcon64.png';
import backPage from './../../assets/img/left-arrow64.png';
import { showMessage } from 'react-native-flash-message';

export class Rota extends Component {
  constructor(props) {
    super(props);

    this.state = {
      form: false,
      rota: {},
    };
  }


  acessoNegado(){
    showMessage({
      message: 'Perfil não autorizado!',
      type: "warning",
      position: "top",
      icon:"warning",
    });
  }

  componentWillMount() {
    this.props.fetchRotas();
  }

  // lista de rotas
  render() {    
    return (
      <Container style={styles.container} testID="rota">
        {this.state.form ? (
          <FormRota
            closeForm={() => this.setState({ form: !this.state.form })}
            rota={this.state.rota}
            {...this.props}
            addRota={this.props.addRota}
          />
        ) : (
          <Container style={styles.container}>
            <View style={{ flex: 1, flexDirection: 'column', width: '100%', justifyContent: "center" }}>

                  <ListRota
                    toggleForm={() => this.setState({ form: !this.state.form })}
                    deleteRota={this.props.deleteRota}
                    setRota={(rota) => this.setState({ rota })}
                    rotas={this.props.rota}
                  />

                  <Button
                      style={{
                        backgroundColor: '#070707',
                        width: '100%',
                        margin: 0,
                        justifyContent: 'center',
                        height:80,
                      }}
                      onPress={() => { global.teste.perfil == 2? this.acessoNegado(): this.setState({ form: !this.state.form })}}
                  >
                      <Image source={addIcon} style={{margin:5 }}/>
                  </Button>
            
            </View>
          </Container>
        )}
      </Container>
    );
  }
}

class FormRota extends Component {
 // cadastro de rotas
  componentDidMount() {
    const { setFieldValue } = this.props;

    setFieldValue('id', this.props.rota.key);
    setFieldValue('titulo', this.props.rota.titulo);
    setFieldValue('origem', this.props.rota.origem);
    setFieldValue('destino', this.props.rota.destino);
    setFieldValue('trajeto', this.props.rota.trajeto);
    setFieldValue('distancia', this.props.rota.distancia);

    // this.setState({
    //   id: this.props.rota.key,
    //   nome: this.props.rota.nome,
    //   cpf: this.props.rota.cpf,
    //   rg: this.props.rota.rg,
    //   dataNascimento: this.props.rota.dataNascimento,
    //   nomeResponsavel: this.props.rota.nomeResponsavel,
    //   telefoneResponsavel: this.props.rota.telefoneResponsavel,
    //   escola: this.props.rota.escola,
    //   endereco: this.props.rota.endereco,
    //   observacoes: this.props.rota.observacoes,
    // });
  }


  render() {
    //const { values, handleSubmit } = this.props;
    const {values, errors, handleSubmit, touched, handleChange} = this.props;
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            maxHeight: 80,
            backgroundColor: '#303030',
          }}
        >
          <TouchableOpacity onPress={this.props.closeForm}>
            <Image source={backPage} style={{marginLeft: 5, marginRight:25}}/>
          </TouchableOpacity>

          <Text style={styles.title}>Rota</Text>
          <Image
            style={styles.logo}
            // eslint-disable-next-line global-require
            source={require('./../../assets/img/bus.png')}
          />
        </View>
        <Container >
          {/*-----------------------------------------------------------------------------------------------------------*/}          
        <ScrollView>
        <Form style={styles.formuario}>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('titulo')}
                  placeholder="titulo"
                  testID="input-titulo"
                  value={values.titulo}
                  style={{ color: '#fff', fontSize:24 }}
                />
                {errors.titulo && touched.titulo && <Text id="feedback">{errors.titulo}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('origem')}
                  placeholder="origem"
                  value={values.origem}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.origem && touched.origem && <Text id="feedback">{errors.origem}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('destino')}
                  placeholder="Destino"
                  value={values.destino}
                  style={{ color: '#fff' , fontSize:24 }}
                />
                {errors.destino && touched.destino && <Text id="feedback">{errors.destino}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('trajeto')}
                  placeholder="Trajeto"
                  value={values.trajeto}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.trajeto && touched.trajeto && <Text id="feedback">{errors.trajeto}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('distancia')}
                  placeholder="0.00"
                  value={values.distancia}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.distancia && touched.distancia && <Text id="feedback">{errors.distancia}</Text>}
              </Item>
              <Button last onPress={handleSubmit} style={styles.button}>
                <Text style={styles.button_text}>Salvar</Text>
              </Button>
        </Form>
        </ScrollView>
{/*-----------------------------------------------------------------------------------------------------------*/}
        </Container>
      </View>
    );
  }
}

class ListRota extends Component {
  constructor(props) {
    super(props);

    this.updateRota = this.updateRota.bind(this);
    this.deleteRota = this.deleteRota.bind(this);
  }

  updateRota(rota) {
    if(global.teste.perfil == 2){
      showMessage({
        message: 'Perfil não autorizado!',
        type: "warning",
        position: "top",
        icon:"warning",
      });
    }
    else{
      this.props.setRota(rota);
      this.props.toggleForm();
    }
  }

  deleteRota(rota) {
    if(global.teste.perfil == 2){
      showMessage({
        message: 'Perfil não autorizado!',
        type: "warning",
        position: "top",
        icon:"warning",
      });
    }
    else{
      this.props.deleteRota(rota);
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <View style={{ flex: 1, flexDirection: 'column', width: '98%' }}>
          <List>
            <ListItem style={{ width: '100%', maxHeight: 92, justifyContent: 'center', backgroundColor: '#303030' }} itemHeader first>
                <Text style={styles.title}>Rota</Text>
                <Image style={styles.logo} source={require('./../../assets/img/bus.png')}/>
            </ListItem>
            <ScrollView>
              {this.props.rotas.map((e) => {
                return (
                  <ListItem style={{ width: '98%',}} key={e.key} icon>
                    <Left style={{ width: '75%', justifyContent: 'flex-start', borderBottomWidth:1, borderColor: '#303030' }}>
                      <Text style={{ color: '#fff', textAlign: 'left', fontSize: 26 }}>
                        {e.titulo}
                      </Text>
                    </Left>
                    <Right
                      style={{ width: '25%', justifyContent: 'space-between', borderBottomWidth:1, borderColor: '#303030'}}
                    >
                      <TouchableOpacity onPress={() => this.updateRota(e)}>
                          <Image source={editIcon} />
                      </TouchableOpacity>

                      <TouchableOpacity onPress={() => this.deleteRota(e)}>
                          <Image source={deleteIcon} />
                      </TouchableOpacity>
                    </Right>
                  </ListItem>
                );
              })}
            </ScrollView>
          </List>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#070707',
    width: Dimensions.get('window').width,
  },
  box_button: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#0F0E0F',
    padding: '5%',
  },
  logo: {
    width: 100,
    height: 100,
  },
  button: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
    marginTop: 30,
    backgroundColor: '#bc9f0b',
  },

  button_text: {
    color: '#fff',
    fontSize: 20,
  },

  formuario: {
    backgroundColor: '#0F0E0F',
    width: Dimensions.get('window').width,
    padding: 10,
    overflow: 'scroll',
    maxHeight: 900
  },

  title: {
    fontSize: 48,
    color: '#fff',
    justifyContent: 'center',
  },

  button_return: {
    color: '#bc9f0b',
    fontSize: 48,

    textAlign: 'left',
    padding: 10,
  },
});

function mapStateToProps(state) {
  return {
    width: Dimensions.get('window').width,
    rota: state.rota.rotas,
  };
}

export default compose(
  connect(
    mapStateToProps,
    { fetchRotas, addRota, deleteRota },
  ),
  withFormik({
// valores inciais nos campos
    mapPropsToValues: () => ({
      titulo: '',
      origem: '',
      destino: '',
      trajeto: '',
      distancia: '',
    }),

    validate: (values) => {
      const errors = {};

      if (!values.titulo) {
        errors.titulo = 'Campo Obrigatório';
      }

      if (!values.origem) {
        errors.origem = 'Campo Obrigatório';
      }

      if (!values.destino) {
        errors.destino = 'Campo Obrigatório';
      }

      if (!values.trajeto) {
        errors.trajeto = 'Campo Obrigatório';
      }

      if (!values.distancia) {
        errors.distancia = 'Campo Obrigatório';
      }

      return errors;
    },
    handleSubmit: (values, { props }) => {
      console.log(values);
      props.addRota(values);
      // props.history.push('/embreagem');
    },
  }),
)(Rota);
