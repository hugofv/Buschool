/* eslint-disable no-undef */
import { render, fireEvent } from 'react-native-testing-library';

import { Provider } from 'react-redux';
import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Rota from './rota';

const mockStore = configureMockStore([thunk]);

const rota = {
  rotas: [],
};

const store = mockStore({ rota });

describe('Rota', () => {
  let props;

  beforeEach(() => {
    props = {
      rota,
      fetchRotas: jest.fn(),
      addRota: jest.fn(),
      deleteRota: jest.fn(),
      handleChange: jest.fn(),
    };
  });

  test('renders the component', () => {
    const { getByTestId } = render(
      <Provider {...{ store }}>
        <Rota {...props} />
      </Provider>,
    );

    expect(getByTestId('rota')).not.toBeNull();
  });
});
