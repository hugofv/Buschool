/* eslint-disable no-undef */
import { render, fireEvent } from 'react-native-testing-library';

import { Provider } from 'react-redux';
import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Veiculo from './veiculo';

const mockStore = configureMockStore([thunk]);

const veiculo = {
  veiculos: [],
};

const store = mockStore({ veiculo });

describe('Veiculo', () => {
  let props;

  beforeEach(() => {
    props = {
      veiculo,
      fetchVeiculos: jest.fn(),
      addVeiculo: jest.fn(),
      deleteVeiculo: jest.fn(),
      handleChange: jest.fn(),
    };
  });

  test('renders the component', () => {
    const { getByTestId } = render(
      <Provider {...{ store }}>
        <Veiculo {...props} />
      </Provider>,
    );

    expect(getByTestId('veiculo')).not.toBeNull();
  });
});
