import { Button, Container, Content, DatePicker, Form, Input, Item, Left, List, ListItem, Right,} from 'native-base';
import {Dimensions, Image, StyleSheet, Text, TouchableOpacity, View, ScrollView, } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withFormik } from 'formik';
import { addAluno, deleteAluno, fetchAlunos } from '../../actions/aluno';
import editIcon from './../../assets/img/edit32.png';
import deleteIcon from './../../assets/img/delete32.png';
import addIcon from './../../assets/img/addIcon64.png';
import backPage from './../../assets/img/left-arrow64.png';
import { showMessage } from 'react-native-flash-message';

export class Aluno extends Component {
  constructor(props) {
    super(props);

    this.state = {
      form: false,
      aluno: {},
    };
  }


  acessoNegado(){
    showMessage({
      message: 'Perfil não autorizado!',
      type: "warning",
      position: "top",
      icon:"warning",
    });
  }

  componentWillMount() {
    this.props.fetchAlunos();
  }

  // lista de alunos
  render() {    
    return (
      <Container style={styles.container} testID="aluno">
        {this.state.form ? (
          <FormAluno
            closeForm={() => this.setState({ form: !this.state.form })}
            aluno={this.state.aluno}
            {...this.props}
            addAluno={this.props.addAluno}
          />
        ) : (
          <Container style={styles.container}>
            <View style={{ flex: 1, flexDirection: 'column', width: '100%', justifyContent: "center" }}>

                  <ListAluno
                    toggleForm={() => this.setState({ form: !this.state.form })}
                    deleteAluno={this.props.deleteAluno}
                    setAluno={(aluno) => this.setState({ aluno })}
                    alunos={this.props.aluno}
                  />

                  <Button
                      testID="button-novo"
                      style={{
                        backgroundColor: '#070707',
                        width: '100%',
                        margin: 0,
                        justifyContent: 'center',
                        height:80,
                      }}
                      onPress={() => { global.teste.perfil == 2? this.acessoNegado(): this.setState({ form: !this.state.form })}}
                  >
                      <Image source={addIcon} style={{margin:5 }}/>
                  </Button>
            
            </View>
          </Container>
        )}
      </Container>
    );
  }
}

class FormAluno extends Component {
 // cadastro de alunos
  componentDidMount() {
    const { setFieldValue } = this.props;

    setFieldValue('id', this.props.aluno.key);
    setFieldValue('nome', this.props.aluno.nome);
    setFieldValue('cpf', this.props.aluno.cpf);
    setFieldValue('rg', this.props.aluno.rg);
    setFieldValue('dataNascimento', this.props.aluno.dataNascimento);
    setFieldValue('nomeResponsavel', this.props.aluno.nomeResponsavel);
    setFieldValue('telefoneResponsavel', this.props.aluno.telefoneResponsavel);
    setFieldValue('escola', this.props.aluno.escola);
    setFieldValue('endereco', this.props.aluno.endereco);
    setFieldValue('observacoes', this.props.aluno.observacoes);

    // this.setState({
    //   id: this.props.aluno.key,
    //   nome: this.props.aluno.nome,
    //   cpf: this.props.aluno.cpf,
    //   rg: this.props.aluno.rg,
    //   dataNascimento: this.props.aluno.dataNascimento,
    //   nomeResponsavel: this.props.aluno.nomeResponsavel,
    //   telefoneResponsavel: this.props.aluno.telefoneResponsavel,
    //   escola: this.props.aluno.escola,
    //   endereco: this.props.aluno.endereco,
    //   observacoes: this.props.aluno.observacoes,
    // });
  }


  render() {
    //const { values, handleSubmit } = this.props;
    const {values, errors, handleSubmit, touched, handleChange} = this.props;
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            maxHeight: 80,
            backgroundColor: '#303030',
          }}
        >
          <TouchableOpacity onPress={this.props.closeForm}>
            <Image source={backPage} style={{marginLeft: 5, marginRight:25}}/>
          </TouchableOpacity>

          <Text style={styles.title}>Aluno</Text>
          <Image
            style={styles.logo}
            // eslint-disable-next-line global-require
            source={require('./../../assets/img/bus.png')}
          />
        </View>
        <Container >
          {/*-----------------------------------------------------------------------------------------------------------*/}          
        <ScrollView>
        <Form style={styles.formuario}>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('nome')}
                  placeholder="Nome"
                  testID="input-nome"
                  value={values.nome}
                  style={{ color: '#fff', fontSize:24 }}
                />
                {errors.nome && touched.nome && <Text id="feedback">{errors.nome}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('cpf')}
                  placeholder="CPF"
                  value={values.cpf}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.cpf && touched.cpf && <Text id="feedback">{errors.cpf}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('rg')}
                  placeholder="RG"
                  value={values.rg}
                  style={{ color: '#fff' , fontSize:24 }}
                />
                {errors.rg && touched.rg && <Text id="feedback">{errors.rg}</Text>}
              </Item>
              <Item>
                <Content>
                  <DatePicker
                    locale="pt"
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType="fade"
                    androidMode="default"
                    placeHolderText="Select date"
                    textStyle={{ color: 'green', fontSize: 24 }}
                    placeHolderTextStyle={{ color: '#d3d3d3', fontSize:24  }}
                    onDateChange={this.props.handleChange('dataNascimento')}
                    disabled={false}
                  />
                  <Text>
    Data de Nascimento:
                    {' '}
                    {values.nascimento}
                  </Text>
                </Content>
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('nomeResponsavel')}
                  placeholder="Nome Responsável"
                  value={values.nomeResponsavel}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.nomeResponsavel && touched.nomeResponsavel && <Text id="feedback">{errors.nomeResponsavel}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('telefoneResponsavel')}
                  placeholder="Telefone Responsável"
                  value={values.telefoneResponsavel}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.telefoneResponsavel && touched.telefoneResponsavel && <Text id="feedback">{errors.telefoneResponsavel}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('escola')}
                  placeholder="Escola"
                  value={values.escola}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.email && touched.escola && <Text id="feedback">{errors.escola}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('endereco')}
                  placeholder="Endereço/Região"
                  value={values.endereco}
                  style={{ color: '#fff' , fontSize:24 }}
                />
                {errors.endereco && touched.endereco && <Text id="feedback">{errors.endereco}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('observacoes')}
                  placeholder="Observações"
                  // eslint-disable-next-line react/prop-types
                  // eslint-disable-next-line react/jsx-no-duplicate-props
                  value={values.observacoes}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.observacoes && touched.observacoes && <Text id="feedback">{errors.observacoes}</Text>}
              </Item>
              <Button last onPress={handleSubmit} style={styles.button}>
                <Text style={styles.button_text}>Salvar</Text>
              </Button>
        </Form>
        </ScrollView>
{/*-----------------------------------------------------------------------------------------------------------*/}
        </Container>
      </View>
    );
  }
}

class ListAluno extends Component {
  constructor(props) {
    super(props);

    this.updateAluno = this.updateAluno.bind(this);
    this.deleteAluno = this.deleteAluno.bind(this);
  }

  updateAluno(aluno) {
    if(global.teste.perfil == 2){
      showMessage({
        message: 'Perfil não autorizado!',
        type: "warning",
        position: "top",
        icon:"warning",
      });
    }
    else{
      this.props.setAluno(aluno);
      this.props.toggleForm();
    }
  }

  deleteAluno(aluno) {
    if(global.teste.perfil == 2){
      showMessage({
        message: 'Perfil não autorizado!',
        type: "warning",
        position: "top",
        icon:"warning",
      });
    }
    else{
      this.props.deleteAluno(aluno);
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <View style={{ flex: 1, flexDirection: 'column', width: '98%' }}>
          <List>
            <ListItem style={{ width: '100%', maxHeight: 92, justifyContent: 'center', backgroundColor: '#303030' }} itemHeader first>
                <Text style={styles.title}>Aluno</Text>
                <Image style={styles.logo} source={require('./../../assets/img/bus.png')}/>
            </ListItem>
            <ScrollView>
              {this.props.alunos.map((e) => {
                return (
                  <ListItem style={{ width: '98%',}} key={e.key} icon>
                    <Left style={{ width: '75%', justifyContent: 'flex-start', borderBottomWidth:1, borderColor: '#303030' }}>
                      <Text style={{ color: '#fff', textAlign: 'left', fontSize: 26 }}>
                        {nome = e.nome.split(' ')[0]}
                      </Text>
                    </Left>
                    <Right
                      style={{ width: '25%', justifyContent: 'space-between', borderBottomWidth:1, borderColor: '#303030'}}
                    >
                      <TouchableOpacity onPress={() => this.updateAluno(e)}>
                          <Image source={editIcon} />
                      </TouchableOpacity>

                      <TouchableOpacity onPress={() => this.deleteAluno(e)}>
                          <Image source={deleteIcon} />
                      </TouchableOpacity>
                    </Right>
                  </ListItem>
                );
              })}
            </ScrollView>
          </List>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#070707',
    width: Dimensions.get('window').width,
  },
  box_button: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#0F0E0F',
    padding: '5%',
  },
  logo: {
    width: 100,
    height: 100,
  },
  button: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
    marginTop: 30,
    backgroundColor: '#bc9f0b',
  },

  button_text: {
    color: '#fff',
    fontSize: 20,
  },

  formuario: {
    backgroundColor: '#0F0E0F',
    width: Dimensions.get('window').width,
    padding: 10,
    overflow: 'scroll'
  },

  title: {
    fontSize: 48,
    color: '#fff',
    justifyContent: 'center',
  },

  button_return: {
    color: '#bc9f0b',
    fontSize: 48,

    textAlign: 'left',
    padding: 10,
  },
});

function mapStateToProps(state) {
  return {
    width: Dimensions.get('window').width,
    aluno: state.aluno.alunos,
  };
}

export default compose(
  connect(
    mapStateToProps,
    { fetchAlunos, addAluno, deleteAluno },
  ),
  withFormik({
// valores inciais nos campos
    mapPropsToValues: () => ({
      nome: '',
      cpf: '',
      rg: '',
      dataNascimento: '',
      nomeResponsavel: '',
      telefoneResponsavel: '',
      escola: '',
      endereco: '',
      observacoes: '',
    }),

    validate: (values) => {
      const errors = {};

      if (!values.nome) {
        errors.nome = 'Campo Obrigatório';
      }

      if (!values.cpf) {
        errors.cpf = 'Campo Obrigatório';
      }

      if (!values.rg) {
        errors.rg = 'Campo Obrigatório';
      }

      if (!values.dataNascimento) {
        errors.dataNascimento = 'Campo Obrigatório';
      }

      if (!values.nomeResponsavel) {
        errors.nomeResponsavel = 'Campo Obrigatório';
      }

      if (!values.telefoneResponsavel) {
        errors.telefoneResponsavel = 'Campo Obrigatório';
      }

      if (!values.escola) {
        errors.escola = 'Campo Obrigatório';
      }

      if (!values.endereco) {
        errors.endereco = 'Campo Obrigatório';
      }

      if (!values.observacoes) {
        errors.observacoes = 'Campo Obrigatório';
      }

      return errors;
    },
    handleSubmit: (values, { props }) => {
      console.log(values);
      props.addAluno(values);
      // props.history.push('/embreagem');
    },
  }),
)(Aluno);
