/* eslint-disable no-undef */
import { render, fireEvent } from 'react-native-testing-library';

import { Provider } from 'react-redux';
import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Motorista from './motorista';

const mockStore = configureMockStore([thunk]);

const motorista = {
  motoristas: [],
};

const store = mockStore({ motorista });

describe('Motorista', () => {
  let props;

  beforeEach(() => {
    props = {
      motorista,
      fetchMotoristas: jest.fn(),
      addMotorista: jest.fn(),
      deleteMotorista: jest.fn(),
      handleChange: jest.fn(),
    };
  });

  test('renders the component', () => {
    const { getByTestId } = render(
      <Provider {...{ store }}>
        <Motorista {...props} />
      </Provider>,
    );

    expect(getByTestId('motorista')).not.toBeNull();
  });
});
