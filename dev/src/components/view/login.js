/* eslint-disable global-require */
/* eslint-disable no-use-before-define */
/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import {
  Button, Container, Form, Input, Item,
} from 'native-base';
import {
  Dimensions, Image, StyleSheet, Text, View,
} from 'react-native';
import React, { Component } from 'react';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { withFormik } from 'formik';
import { autenticar } from '../../actions/login';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
    this.login = this.login.bind(this);
  }

  login() {
    const { email, password } = this.state;
    const { autenticar } = this.props;
    autenticar(email, password);
  }

  render() {
    const {
      values, errors, handleSubmit, touched, handleChange,
    } = this.props;
    return (
      <Container style={styles.container}>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: -80,
              marginTop: 50,
            }}
          >
            <Text style={styles.title}>BuSchool</Text>
            <Image style={styles.logo} source={require('./../../assets/img/bus.png')} />
          </View>
          <Form style={styles.formuario}>
            <Item>
              <Input
                placeholder="Email"
                onChangeText={handleChange('email')}
                value={values.email}
              />
              {errors.email && touched.email && <Text id="feedback">{errors.email}</Text>}
            </Item>
            <Item>
              <Input
                placeholder="Senha"
                secureTextEntry
                onChangeText={handleChange('password')}
                value={values.password}
              />
              {errors.password && touched.password && <Text id="feedback">{errors.password}</Text>}
            </Item>

            <Button last onPress={handleSubmit} style={styles.button}>
              <Text style={styles.button_text}>Entrar</Text>
            </Button>
          </Form>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#303030',
    width: Dimensions.get('window').width,
  },
  logo: {
    width: 100,
    height: 100,
  },

  button: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
    marginTop: 30,
    backgroundColor: '#bc9f0b',
  },

  button_text: {
    color: '#fff',
    fontSize: 20,
  },

  title: {
    fontSize: 48,
    color: '#fff',
    justifyContent: 'center',
  },

  formuario: {
    backgroundColor: '#424242',
    width: Dimensions.get('window').width,
    marginBottom: 100,
    padding: 10,
    paddingTop: 50,
    flex: 1,
    // flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center'
  },
});

function mapStateToProps(state) {
  return {
    usuario: state.usuario,
  };
}

export default compose(
  connect(
    mapStateToProps,
    { autenticar },
  ),
  withFormik({
    mapPropsToValues: () => ({
      email: '',
      password: '',
    }),

    validate: (values) => {
      const errors = {};

      if (!values.email) {
        errors.email = 'Campo Obrigatório';
      }

      if (!values.password) {
        errors.password = 'Campo Obrigatório';
      }

      return errors;
    },

    handleSubmit: (values, { props }) => {
      props.autenticar(values.email, values.password);
    },
  }),
)(Login);
