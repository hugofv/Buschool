/* eslint-disable no-undef */
import { render, fireEvent } from 'react-native-testing-library';

import { Provider } from 'react-redux';
import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Aluno from './aluno';

const mockStore = configureMockStore([thunk]);

const aluno = {
  alunos: [],
};

const store = mockStore({ aluno });

describe('Aluno', () => {
  let props;

  beforeEach(() => {
    props = {
      aluno,
      fetchAlunos: jest.fn(),
      addAluno: jest.fn(),
      deleteAluno: jest.fn(),
      handleChange: jest.fn(),
    };
  });

  test('renders the component', () => {
    const { getByTestId } = render(
      <Provider {...{ store }}>
        <Aluno {...props} />
      </Provider>,
    );

    expect(getByTestId('aluno')).not.toBeNull();
  });

  // test('handle input nome', async () => {
  //   const { getByTestId } = render(
  //     <Provider {...{ store }}>
  //       <Aluno {...props} />
  //     </Provider>
  //   );

  //   fireEvent.press(getByTestId('button-novo'))
  //   const input = getByTestId('input-nome')

  //   fireEvent.changeText(input, { target: { value: 'teste' } })
  //   expect(input.value).toBe('teste')
  // })
});
