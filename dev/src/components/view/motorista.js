import { Button, Container, Content, DatePicker, Form, Input, Item, Left, List, ListItem, Right,} from 'native-base';
import {Dimensions, Image, StyleSheet, Text, TouchableOpacity, View, ScrollView, } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withFormik } from 'formik';
import { addMotorista, deleteMotorista, fetchMotoristas } from '../../actions/motorista';
import editIcon from './../../assets/img/edit32.png';
import deleteIcon from './../../assets/img/delete32.png';
import addIcon from './../../assets/img/addIcon64.png';
import backPage from './../../assets/img/left-arrow64.png';
import { showMessage } from 'react-native-flash-message';

export class Motorista extends Component {
  constructor(props) {
    super(props);

    this.state = {
      form: false,
      motorista: {},
    };
  }

  acessoNegado(){
    showMessage({
      message: 'Perfil não autorizado!',
      type: "warning",
      position: "top",
      icon:"warning",
    });
  }

  componentWillMount() {
    this.props.fetchMotoristas();
  }

  // lista de motoristas
  render() {    
    return (
      <Container style={styles.container} testID="motorista">
        {this.state.form ? (
          <FormMotorista
            closeForm={() => this.setState({ form: !this.state.form })}
            motorista={this.state.motorista}
            {...this.props}
            addMotorista={this.props.addMotorista}
          />
        ) : (
          <Container style={styles.container}>
            <View style={{ flex: 1, flexDirection: 'column', width: '100%', justifyContent: "center" }}>

                  <ListMotorista
                    toggleForm={() => this.setState({ form: !this.state.form })}
                    deleteMotorista={this.props.deleteMotorista}
                    setMotorista={(motorista) => this.setState({ motorista })}
                    motoristas={this.props.motorista}
                  />

                  <Button
                      style={{
                        backgroundColor: '#070707',
                        width: '100%',
                        margin: 0,
                        justifyContent: 'center',
                        height:80,
                      }}
                      onPress={() => { global.teste.perfil == 2? this.acessoNegado(): this.setState({ form: !this.state.form })}}
                  >
                      <Image source={addIcon} style={{margin:5 }}/>
                  </Button>
            
            </View>
          </Container>
        )}
      </Container>
    );
  }
}

class FormMotorista extends Component {
 // cadastro de motoristas
  componentDidMount() {
    const { setFieldValue } = this.props;

    setFieldValue('id', this.props.motorista.key);
    setFieldValue('nome', this.props.motorista.nome);
    setFieldValue('cpf', this.props.motorista.cpf);
    setFieldValue('cnh', this.props.motorista.cnh);
    setFieldValue('dataNascimento', this.props.motorista.dataNascimento);
    setFieldValue('telefone', this.props.motorista.telefone);
    setFieldValue('observacoes', this.props.motorista.observacoes);

    // this.setState({
    //   id: this.props.motorista.key,
    //   nome: this.props.motorista.nome,
    //   cpf: this.props.motorista.cpf,
    //   rg: this.props.motorista.rg,
    //   dataNascimento: this.props.motorista.dataNascimento,
    //   nomeResponsavel: this.props.motorista.nomeResponsavel,
    //   telefone: this.props.motorista.telefone,
    //   escola: this.props.motorista.escola,
    //   endereco: this.props.motorista.endereco,
    //   observacoes: this.props.motorista.observacoes,
    // });
  }


  render() {
    //const { values, handleSubmit } = this.props;
    const {values, errors, handleSubmit, touched, handleChange} = this.props;
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            maxHeight: 80,
            backgroundColor: '#303030',
          }}
        >
          <TouchableOpacity onPress={this.props.closeForm}>
            <Image source={backPage} style={{marginLeft: 5, marginRight:25}}/>
          </TouchableOpacity>

          <Text style={styles.title}>Motorista</Text>
          <Image
            style={styles.logo}
            // eslint-disable-next-line global-require
            source={require('./../../assets/img/bus.png')}
          />
        </View>
        <Container>
          {/*-----------------------------------------------------------------------------------------------------------*/}          
        <ScrollView>
        <Form style={styles.formuario}>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('nome')}
                  placeholder="Nome"
                  testID="input-nome"
                  value={values.nome}
                  style={{ color: '#fff', fontSize:24 }}
                />
                {errors.nome && touched.nome && <Text id="feedback">{errors.nome}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('cpf')}
                  placeholder="CPF"
                  value={values.cpf}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.cpf && touched.cpf && <Text id="feedback">{errors.cpf}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('cnh')}
                  placeholder="CNH"
                  value={values.cnh}
                  style={{ color: '#fff' , fontSize:24 }}
                />
                {errors.cnh && touched.cnh && <Text id="feedback">{errors.cnh}</Text>}
              </Item>
              <Item>
                <Content>
                  <DatePicker
                    locale="pt"
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType="fade"
                    androidMode="default"
                    placeHolderText="Select date"
                    textStyle={{ color: 'green', fontSize: 24 }}
                    placeHolderTextStyle={{ color: '#d3d3d3', fontSize:24  }}
                    onDateChange={this.props.handleChange('dataNascimento')}
                    disabled={false}
                  />
                  <Text>
    Data de Nascimento:
                    {' '}
                    {values.nascimento}
                  </Text>
                </Content>
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('telefone')}
                  placeholder="Telefone"
                  value={values.telefone}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.telefone && touched.telefone && <Text id="feedback">{errors.telefone}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('observacoes')}
                  placeholder="Observações"
                  // eslint-disable-next-line react/prop-types
                  // eslint-disable-next-line react/jsx-no-duplicate-props
                  value={values.observacoes}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {!errors.observacoes && touched.observacoes && <Text id="feedback">{errors.observacoes}</Text>}
              </Item>
              <Button last onPress={handleSubmit} style={styles.button}>
                <Text style={styles.button_text}>Salvar</Text>
              </Button>
        </Form>
        </ScrollView>
{/*-----------------------------------------------------------------------------------------------------------*/}
        </Container>
      </View>
    );
  }
}

class ListMotorista extends Component {
  constructor(props) {
    super(props);

    this.updateMotorista = this.updateMotorista.bind(this);
    this.deleteMotorista = this.deleteMotorista.bind(this);
  }

  updateMotorista(motorista) {

    if(global.teste.perfil == 2){
      showMessage({
        message: 'Perfil não autorizado!',
        type: "warning",
        position: "top",
        icon:"warning",
      });
    }
    else{
      this.props.setMotorista(motorista);
      this.props.toggleForm();
    }

  }

  deleteMotorista(motorista) {
    if(global.teste.perfil == 2){
      showMessage({
        message: 'Perfil não autorizado!',
        type: "warning",
        position: "top",
        icon:"warning",
      });
    }
    else{
      this.props.deleteMotorista(motorista);
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <View style={{ flex: 1, flexDirection: 'column', width: '98%' }}>
          <List>
            <ListItem style={{ width: '100%', maxHeight: 92, justifyContent: 'center', backgroundColor: '#303030' }} itemHeader first>
                <Text style={styles.title}>Motorista</Text>
                <Image style={styles.logo} source={require('./../../assets/img/bus.png')}/>
            </ListItem>
            <ScrollView>
              {this.props.motoristas.map((e) => {
                return (
                  <ListItem style={{ width: '98%',}} key={e.key} icon>
                    <Left style={{ width: '75%', justifyContent: 'flex-start', borderBottomWidth:1, borderColor: '#303030' }}>
                      <Text style={{ color: '#fff', textAlign: 'left', fontSize: 26 }}>
                        {nome = e.nome.split(' ')[0]}
                      </Text>
                    </Left>
                    <Right
                      style={{ width: '25%', justifyContent: 'space-between', borderBottomWidth:1, borderColor: '#303030'}}
                    >
                      <TouchableOpacity onPress={() => this.updateMotorista(e)}>
                           <Image source={editIcon}/>
                      </TouchableOpacity>

                      <TouchableOpacity onPress={() => this.deleteMotorista(e)}>
                          <Image source={deleteIcon} />
                      </TouchableOpacity>
                    </Right>
                  </ListItem>
                );
              })}
            </ScrollView>
          </List>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#070707',
    width: Dimensions.get('window').width
  },
  box_button: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#0F0E0F',
    padding: '5%',
  },
  logo: {
    width: 100,
    height: 100,
  },
  button: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
    marginTop: 30,
    backgroundColor: '#bc9f0b',
  },

  button_text: {
    color: '#fff',
    fontSize: 20,
  },

  formuario: {
    backgroundColor: '#0F0E0F',
    width: Dimensions.get('window').width,
    padding: 10,
    height: Dimensions.get('window').height
  },

  title: {
    fontSize: 48,
    color: '#fff',
    justifyContent: 'center',
  },

  button_return: {
    color: '#bc9f0b',
    fontSize: 48,

    textAlign: 'left',
    padding: 10,
  },
});

function mapStateToProps(state) {
  return {
    width: Dimensions.get('window').width,
    motorista: state.motorista.motoristas,
  };
}


export default compose(
  connect(
    mapStateToProps,
    { fetchMotoristas, addMotorista, deleteMotorista },
  ),
  withFormik({
// valores inciais nos campos
    mapPropsToValues: () => ({
      nome: '',
      cpf: '',
      cnh: '',
      dataNascimento: '',
      telefone: '',
      observacoes: '',
    }),

    validate: (values) => {
      
      const errors = {};

      if (!values.nome) {
        errors.nome = 'Campo Obrigatório';
      }
 
      if (!values.cpf) {
        errors.cpf = 'Campo Obrigatório';
      }

      if (!values.cnh) {
        errors.cnh = 'Campo Obrigatório';
      }

      if (!values.dataNascimento) {
        errors.dataNascimento = 'Campo Obrigatório';
      }

      if (!values.telefone) {
        errors.telefone = 'Campo Obrigatório';
      }

      if (!values.observacoes) {
        errors.observacoes = 'Campo Obrigatório';
      }

      return errors;
    },

    handleSubmit: (values, { props }) => {
      props.addMotorista(values);
      // props.history.push('/embreagem');
    },
  }),
)(Motorista);
