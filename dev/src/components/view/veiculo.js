import { Button, Container, Content, DatePicker, Form, Input, Item, Left, List, ListItem, Right,} from 'native-base';
import {Dimensions, Image, StyleSheet, Text, TouchableOpacity, View, ScrollView, } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withFormik } from 'formik';
import { addVeiculo, deleteVeiculo, fetchVeiculos } from '../../actions/veiculo';
import editIcon from './../../assets/img/edit32.png';
import deleteIcon from './../../assets/img/delete32.png';
import addIcon from './../../assets/img/addIcon64.png';
import backPage from './../../assets/img/left-arrow64.png';
import { showMessage } from 'react-native-flash-message';

export class Veiculo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      form: false,
      veiculo: {},
    };
  }


  acessoNegado(){
    showMessage({
      message: 'Perfil não autorizado!',
      type: "warning",
      position: "top",
      icon:"warning",
    });
  }

  componentWillMount() {
    this.props.fetchVeiculos();
  }

  // lista de veiculos
  render() {    
    return (
      <Container style={styles.container} testID="veiculo">
        {this.state.form ? (
          <FormVeiculo
            closeForm={() => this.setState({ form: !this.state.form })}
            veiculo={this.state.veiculo}
            {...this.props}
            addVeiculo={this.props.addVeiculo}
          />
        ) : (
          <Container style={styles.container}>
            <View style={{ flex: 1, flexDirection: 'column', width: '100%', justifyContent: "center" }}>

                  <ListVeiculo
                    toggleForm={() => this.setState({ form: !this.state.form })}
                    deleteVeiculo={this.props.deleteVeiculo}
                    setVeiculo={(veiculo) => this.setState({ veiculo })}
                    veiculos={this.props.veiculo}
                  />

                  <Button
                      style={{
                        backgroundColor: '#070707',
                        width: '100%',
                        margin: 0,
                        justifyContent: 'center',
                        height:80,
                      }}
                      onPress={() => { global.teste.perfil == 2? this.acessoNegado(): this.setState({ form: !this.state.form })}}
                  >
                      <Image source={addIcon} style={{margin:5 }}/>
                  </Button>
            
            </View>
          </Container>
        )}
      </Container>
    );
  }
}

class FormVeiculo extends Component {
 // cadastro de veiculos
  componentDidMount() {
    const { setFieldValue } = this.props;

    setFieldValue('id', this.props.veiculo.key);
    setFieldValue('marca', this.props.veiculo.marca);
    setFieldValue('modelo', this.props.veiculo.modelo);
    setFieldValue('anoFabricacao', this.props.veiculo.anoFabricacao);
    setFieldValue('patrimonio', this.props.veiculo.patrimonio);
    setFieldValue('cor', this.props.veiculo.cor);
    setFieldValue('placa', this.props.veiculo.placa);

    // this.setState({
    //   id: this.props.veiculo.key,
    //   nome: this.props.veiculo.nome,
    //   cpf: this.props.veiculo.cpf,
    //   rg: this.props.veiculo.rg,
    //   dataNascimento: this.props.veiculo.dataNascimento,
    //   nomeResponsavel: this.props.veiculo.nomeResponsavel,
    //   telefoneResponsavel: this.props.veiculo.telefoneResponsavel,
    //   escola: this.props.veiculo.escola,
    //   endereco: this.props.veiculo.endereco,
    //   observacoes: this.props.veiculo.observacoes,
    // });
  }


  render() {
    //const { values, handleSubmit } = this.props;
    const {values, errors, handleSubmit, touched, handleChange} = this.props;
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            maxHeight: 80,
            backgroundColor: '#303030',
          }}
        >
          <TouchableOpacity onPress={this.props.closeForm}>
            <Image source={backPage} style={{marginLeft: 5, marginRight:25}}/>
          </TouchableOpacity>

          <Text style={styles.title}>Onibus</Text>
          <Image
            style={styles.logo}
            // eslint-disable-next-line global-require
            source={require('./../../assets/img/bus.png')}
          />
        </View>
        <Container >
          {/*-----------------------------------------------------------------------------------------------------------*/}          
        <ScrollView>
        <Form style={styles.formuario}>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('marca')}
                  placeholder="Marca"
                  testID="input-marca"
                  value={values.marca}
                  style={{ color: '#fff', fontSize:24 }}
                />
                {errors.marca && touched.marca && <Text id="feedback">{errors.marca}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('modelo')}
                  placeholder="Modelo"
                  value={values.modelo}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.modelo && touched.modelo && <Text id="feedback">{errors.modelo}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('anoFabricacao')}
                  placeholder="Ano de Fabricação"
                  value={values.anoFabricacao}
                  style={{ color: '#fff' , fontSize:24 }}
                />
                {errors.anoFabricacao && touched.anoFabricacao && <Text id="feedback">{errors.anoFabricacao}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('patrimonio')}
                  placeholder="Patrimônio"
                  value={values.patrimonio}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.patrimonio && touched.patrimonio && <Text id="feedback">{errors.patrimonio}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('cor')}
                  placeholder="Cor do Veículo"
                  value={values.cor}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.cor && touched.cor && <Text id="feedback">{errors.cor}</Text>}
              </Item>
              <Item>
                <Input
                  onChangeText={this.props.handleChange('placa')}
                  placeholder="Placa"
                  value={values.placa}
                  style={{ color: '#fff', fontSize:24  }}
                />
                {errors.placa && touched.placa && <Text id="feedback">{errors.placa}</Text>}
              </Item>
              <Button last onPress={handleSubmit} style={styles.button}>
                <Text style={styles.button_text}>Salvar</Text>
              </Button>
        </Form>
        </ScrollView>
{/*-----------------------------------------------------------------------------------------------------------*/}
        </Container>
      </View>
    );
  }
}

class ListVeiculo extends Component {
  constructor(props) {
    super(props);

    this.updateVeiculo = this.updateVeiculo.bind(this);
    this.deleteVeiculo = this.deleteVeiculo.bind(this);
  }

  updateVeiculo(veiculo) {
    if(global.teste.perfil == 2){
      showMessage({
        message: 'Perfil não autorizado!',
        type: "warning",
        position: "top",
        icon:"warning",
      });
    }
    else{
      this.props.setVeiculo(veiculo);
      this.props.toggleForm();
    }
  }

  deleteVeiculo(veiculo) {
    if(global.teste.perfil == 2){
      showMessage({
        message: 'Perfil não autorizado!',
        type: "warning",
        position: "top",
        icon:"warning",
      });
    }
    else{
      this.props.deleteVeiculo(veiculo);
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <View style={{ flex: 1, flexDirection: 'column', width: '98%' }}>
          <List>
            <ListItem style={{ width: '100%', maxHeight: 92, justifyContent: 'center', backgroundColor: '#303030' }} itemHeader first>
                <Text style={styles.title}>Onibus</Text>
                <Image style={styles.logo} source={require('./../../assets/img/bus.png')}/>
            </ListItem>
            <ScrollView>
              {this.props.veiculos.map((e) => {
                return (
                  <ListItem style={{ width: '98%',}} key={e.key} icon>
                    <Left style={{ width: '75%', justifyContent: 'flex-start', borderBottomWidth:1, borderColor: '#303030' }}>
                      <Text style={{ color: '#fff', textAlign: 'left', fontSize: 26 }}>
                        {`${e.modelo} - ${e.placa}`}
                      </Text>
                    </Left>
                    <Right
                      style={{ width: '25%', justifyContent: 'space-between', borderBottomWidth:1, borderColor: '#303030'}}
                    >
                      <TouchableOpacity onPress={() => this.updateVeiculo(e)}>
                          <Image source={editIcon} />
                      </TouchableOpacity>

                      <TouchableOpacity onPress={() => this.deleteVeiculo(e)}>
                          <Image source={deleteIcon} />
                      </TouchableOpacity>
                    </Right>
                  </ListItem>
                );
              })}
            </ScrollView>
          </List>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#070707',
    width: Dimensions.get('window').width,
  },
  box_button: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#0F0E0F',
    padding: '5%',
  },
  logo: {
    width: 100,
    height: 100,
  },
  button: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
    marginTop: 30,
    backgroundColor: '#bc9f0b',
  },

  button_text: {
    color: '#fff',
    fontSize: 20,
  },

  formuario: {
    backgroundColor: '#0F0E0F',
    width: Dimensions.get('window').width,
    padding: 10,
    overflow: 'scroll',
    maxHeight: 900
  },

  title: {
    fontSize: 48,
    color: '#fff',
    justifyContent: 'center',
  },

  button_return: {
    color: '#bc9f0b',
    fontSize: 48,

    textAlign: 'left',
    padding: 10,
  },
});

function mapStateToProps(state) {
  return {
    width: Dimensions.get('window').width,
    veiculo: state.veiculo.veiculos,
  };
}

export default compose(
  connect(
    mapStateToProps,
    { fetchVeiculos, addVeiculo, deleteVeiculo },
  ),
  withFormik({
// valores inciais nos campos
    mapPropsToValues: () => ({
      marca: '',
      modelo: '',
      anoFabricacao: '',
      patrimonio: '',
      cor: '',
      placa: '',
    }),

    validate: (values) => {
      const errors = {};

      if (!values.marca) {
        errors.marca = 'Campo Obrigatório';
      }

      if (!values.modelo) {
        errors.modelo = 'Campo Obrigatório';
      }

      if (!values.anoFabricacao) {
        errors.anoFabricacao = 'Campo Obrigatório';
      }

      if (!values.patrimonio) {
        errors.patrimonio = 'Campo Obrigatório';
      }

      if (!values.cor) {
        errors.cor = 'Campo Obrigatório';
      }
      if (!values.placa) {
        errors.placa = 'Campo Obrigatório';
      }

      return errors;
    },
    handleSubmit: (values, { props }) => {
      console.log(values);
      props.addVeiculo(values);
      // props.history.push('/embreagem');
    },
  }),
)(Veiculo);
