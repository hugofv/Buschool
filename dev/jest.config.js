module.exports = {
  verbose: true,
  automock: false,
  browser: false,
  bail: false,
  preset: 'react-native',
  roots: ['<rootDir>'],
  testPathIgnorePatterns: ['lib/', 'node_modules/'],
  transformIgnorePatterns: ['!node_modules/react-runtime'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  transform: {
    '^.+\\.js?$': 'babel-jest',
  },
};
